This is a math project on the topic of quantizers eleimination.

report.pdf contains our work on the theoric aspect and our implementation details.

./implementation contains an implementation of the theory developped in report.pdf.
It's written in Ocaml, to compile it, execute the ./implementation/compile.sh script.
To execute it execute the ./implementation/eliminate script.

Some example are already shown. If you want to add your own ones, you will need to get into the code of ./implementation/main.ml.