
(*
  VOICI QUELQUES SUPPOSITIONS A FAIRE SUR LES FORMULE POUR LE MOMENT :
  - Formule sous forme prénexe
 *)

open Typage
open Debug
open List
open Arithm;;



(*-----------------------------------------------------------------------------------------*)



(*---------------------------------------------Simplification---------------------------------------------*)
let simplifier_and a b =
  match a, b with
  | NONE, _ -> b
  | _, NONE -> a
  | ATOM(FALSE), _ -> ATOM(FALSE)
  | _, ATOM(FALSE) -> ATOM(FALSE)
  | ATOM(TRUE), _ -> b
  | _, ATOM(TRUE) -> a
  | _, _ -> AND(a, b)


let simplifier_or a b =
  match a, b with
  | NONE, _ -> b
  | _, NONE -> a
  | ATOM(FALSE), _ -> b
  | _, ATOM(FALSE) -> a
  | ATOM(TRUE), _ -> ATOM(TRUE)
  | _, ATOM(TRUE) -> ATOM(TRUE)
  | _, _ -> AND(a, b)
                  


let rec simplifier_formule form =
  match form with
  | NONE -> NONE
  | ATOM(a) -> ATOM(a)
  | NOT(a) -> NOT(simplifier_formule a)
  | AND(a, b) -> simplifier_and (simplifier_formule a) (simplifier_formule b)
  | OR(a, b) -> simplifier_or (simplifier_formule a) (simplifier_formule b)
 
let rec simplifier_prenex formule =
  match formule with
  |FREE(a) -> FREE(simplifier_formule a)
  |NOT(a) -> NOT(simplifier_prenex a)
  | _ -> failwith "On simplifie après élimination uniquement"



(*---------------------------------------------------------------------EVALUATION--------------------------------------------*)
let calc a =
  let rec aux a acc =
    match a with
    | CONST(a) -> a+acc
    | ADD(x, y) -> aux x (aux y acc)
    | MULCONST(x, y) -> x * aux y acc
    | _ -> failwith "encore des variables au cours de l'évaluation"
  in aux a 0
   
       
let evaluer_relation rela =
  match rela with
  | TRUE -> TRUE
  | FALSE -> FALSE
  | HIGHER (a, b) -> if calc a < calc b then TRUE else FALSE
  | LOWER (a, b) -> if calc a > calc b then TRUE else FALSE 
  | EQUAL (a, b) -> if calc a = calc b then TRUE else FALSE
  | DIVIDE (a, b) -> if ((calc b) mod a=0) then TRUE else FALSE;;

       
let rec evaluer_formule form =
  match form with
  | NONE -> NONE
  | ATOM(a) -> ATOM(evaluer_relation a)
  | NOT(a) -> NOT(evaluer_formule a)
  | AND(a, b) -> AND ((evaluer_formule a), (evaluer_formule b))
  | OR(a, b) -> OR ((evaluer_formule a), (evaluer_formule b))


              
let rec evaluer_prenex formule =
  print_string(display_prenex formule);
  match formule with
  |FREE(a) -> FREE(evaluer_formule a)
  |NOT(a) -> NOT(evaluer_prenex a)
  | _ -> failwith "On évalue après élimination uniquement"

(*-----------------------------------------------------------------------------------------------FIN EVALUATION---------------------------------------------*)

let rec contient_var exp =
  match exp with
    | CONST(a) -> false
    | ADD(x, y) -> (contient_var x )||(contient_var y)
    | MULCONST(x, y) -> contient_var y
    | _ -> true

let calc2 a =
  if contient_var a
  then a
  else
    let rec aux a acc =
      match a with
      | CONST(k) -> k+acc
      | ADD(x, y) -> aux x (aux y acc)
      | MULCONST(x, y) -> x * aux y acc
      | _ -> failwith "encore des variables au cours de l'évaluation"
    in CONST(aux a 0)
   
   
let rec reduire_relation rela =
  match rela with
  | TRUE -> TRUE
  | FALSE -> FALSE
  | HIGHER (a, b) -> HIGHER(calc2 a, calc2 b)
  | LOWER (a, b) -> LOWER(calc2 a, calc2 b)
  | EQUAL (a, b) -> EQUAL(calc2 a, calc2 b)
  | DIVIDE (a, b) -> DIVIDE( a, calc2 b)
                     
let rec reduire_formule form =
  match form with
  | NONE -> NONE
  | ATOM(a) -> ATOM(reduire_relation a)
  | NOT(a) -> NOT(reduire_formule a)
  | AND(a, b) -> AND ((reduire_formule a), (reduire_formule b))
  | OR(a, b) -> OR ((reduire_formule a), (reduire_formule b))

(*-----------------------------------------------------------Code première version--------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------*)

       
(*Retourne true si les deux expressions sont les meme*)
let rec exp_same exp1 exp2 = 
  match exp1, exp2 with
  | VAR(x), VAR(y) -> (String.compare x y) == 0
  | CONST(i), CONST(j) -> i == j
  | MULCONST(i, e1), MULCONST(j, e2) ->
     if i==j then (exp_same e1 e2)
     else false
  | ADD(e1, e2), ADD(o1, o2) -> (exp_same e1 o1) && (exp_same e2 o2)
  | _ -> false;;

(*Remplace dans l'expression exp les occurences de l'expression x par l'expression rep*)
let rec replace_var_x_expression x rep exp = 
  match exp with
  | CONST(i) -> CONST(i)
  | MULCONST(i, a) -> MULCONST(i,replace_var_x_expression x rep a)
  | VAR(a) ->
     if (exp_same (VAR(a)) x) then
       rep
     else
       VAR(a)
  | ADD(a, b) -> ADD(replace_var_x_expression x rep a, replace_var_x_expression x rep b);;

(*Remplace dans la formule f les occurences de l'expression x par l'expression rep*)
let rec replace_var_x_formule x rep f = 
  match f with
  | NONE -> NONE
  | NOT(a) -> replace_var_x_formule x rep a
  | AND(a, b) -> AND( (replace_var_x_formule x rep a), (replace_var_x_formule x rep b))
  | OR(a, b) -> OR( (replace_var_x_formule x rep a), (replace_var_x_formule x rep b))
  | ATOM(r) ->
     match r with
     | HIGHER(a, b) -> ATOM(HIGHER( (replace_var_x_expression x rep a), (replace_var_x_expression x rep b)) )
     | LOWER(a, b) -> ATOM(LOWER( (replace_var_x_expression x rep a), (replace_var_x_expression x rep b)) )
     | EQUAL(a, b) -> ATOM(EQUAL( (replace_var_x_expression x rep a), (replace_var_x_expression x rep b)) )
     | DIVIDE(i, b) -> ATOM(DIVIDE(i, (replace_var_x_expression x rep b)) )
     | TRUE -> ATOM(TRUE)
     | FALSE -> ATOM(FALSE);;


(*Retourne la liste des coefficients de la variable x dans l'expression exp*)
let rec get_var_coef_from_expression x exp = 
  match exp with
  | CONST(i) -> []
  | MULCONST(i, a) ->
     if exp_same a x then
       [i]
     else
       []
  | VAR(a) -> []
  | ADD(a, b) -> (get_var_coef_from_expression x a) @ (get_var_coef_from_expression x b);;

(*Retourne la liste des coefficients de la variable x dans la formule f*)
let rec get_var_coef_from_formule x f = 
  match f with
  | NONE -> []
  | NOT(a) -> get_var_coef_from_formule x a
  | AND(a, b) -> (get_var_coef_from_formule x a) @ (get_var_coef_from_formule x b)
  | OR(a, b) -> (get_var_coef_from_formule x a) @ (get_var_coef_from_formule x b)
  | ATOM(r) -> 
     match r with
     | HIGHER(a, b) -> (get_var_coef_from_expression x a) @ (get_var_coef_from_expression x b)
     | LOWER(a, b) -> (get_var_coef_from_expression x a) @ (get_var_coef_from_expression x b)
     | DIVIDE(i, b) -> (get_var_coef_from_expression x b)
     | EQUAL(a, b) -> (get_var_coef_from_expression x a) @ (get_var_coef_from_expression x b)
     | _ -> []




(*Retourne true si l'expression exp contient la variable x*)
let rec expression_contient_x x exp = 
  match exp with
  | CONST(i) -> false
  | MULCONST(i, a) ->
     if exp_same a x then
       true
     else
       false
  | VAR(a) -> 
     if exp_same (VAR(a)) x then
       true
     else
       false
  | ADD(a, b) -> (expression_contient_x x a) || (expression_contient_x x b);;

(*Retourne true si la relation rel contient la variable x*)
let relation_contient_x x rel = 
  match rel with
  | HIGHER(a, b) -> (expression_contient_x x a) || (expression_contient_x x b)
  | LOWER(a, b) -> (expression_contient_x x a) || (expression_contient_x x b)
  | EQUAL(a, b) -> (expression_contient_x x a) || (expression_contient_x x b)
  | DIVIDE(i, b) -> (expression_contient_x x b)
  | _ -> false




          
(*Multipli toutes les variables et constantes de l'expression exp par coef*)
let rec mult_expression_coef coef exp = 
  match exp with
  | CONST(i) -> CONST(i*coef)
  | MULCONST(i, a) -> MULCONST(i*coef, a)
  | VAR(a) -> MULCONST(coef, VAR(a))
  | ADD(a, b) -> ADD((mult_expression_coef coef a), (mult_expression_coef coef b))
               
(*Multipli toutes les variables et constantes de la relation rel par coef*)
let rec mult_relation_coef coef rel = 
  match rel with
  | HIGHER(a, b) -> HIGHER( (mult_expression_coef coef a), (mult_expression_coef coef b) )
  | LOWER(a, b) -> LOWER( (mult_expression_coef coef a), (mult_expression_coef coef b) )
  | EQUAL(a, b) -> EQUAL( (mult_expression_coef coef a), (mult_expression_coef coef b) )
  | DIVIDE(i, b) -> DIVIDE( i*coef, (mult_expression_coef coef b) )
  | TRUE -> TRUE
  | FALSE -> FALSE
           
(*Multipli toutes les variables et constantes de la formule f par coef*)
let rec mult_x_relation_in_formule coef x f = 
  match f with    
  | NONE -> NONE
  | NOT(a) -> NOT(mult_x_relation_in_formule coef x a)
  | AND(a, b) -> AND( (mult_x_relation_in_formule coef x a), (mult_x_relation_in_formule coef x b) )
  | OR(a, b) -> OR( (mult_x_relation_in_formule coef x a), (mult_x_relation_in_formule coef x b) )
  | ATOM(r) -> 
     if relation_contient_x x r then
       ATOM( mult_relation_coef coef r )
     else
       ATOM(r)


(*remplace les occurances de coef*x par x dans l'expression exp*)
let rec replace_expression_coef_x coef x exp = 
  if coef == 1 then exp
  else
    match exp with
    | CONST(i) -> CONST(i)
    | MULCONST(i, a) -> 
       if (exp_same a x) && ((i mod coef) == 0) then
	 a
       else
	 MULCONST(i, a)
    | VAR(a) -> VAR(a)
    | ADD(a, b) -> ADD(replace_expression_coef_x coef x a, replace_expression_coef_x coef x b)
                 
(*remplace les occurances de coef*x par x dans la relation rel*)
let replace_relation_coef_x coef x rel = 
  match rel with
  | HIGHER(a, b) -> HIGHER( (replace_expression_coef_x coef x a), (replace_expression_coef_x coef x b) )
  | LOWER(a, b) -> LOWER( (replace_expression_coef_x coef x a), (replace_expression_coef_x coef x b) )
  | EQUAL(a, b) -> EQUAL( (replace_expression_coef_x coef x a), (replace_expression_coef_x coef x b) )
  | DIVIDE(i, b) -> DIVIDE(i, (replace_expression_coef_x coef x b) )
  | TRUE -> TRUE
  | FALSE -> FALSE
           
(*remplace les occurences de coef*x par x dans la formule f*)
let rec remplace_formule_coef_x coef x f = 
  match f with
  | NONE -> NONE
  | NOT(a) -> NOT(remplace_formule_coef_x coef x a)
  | AND(a, b) -> AND( (remplace_formule_coef_x coef x a), (remplace_formule_coef_x coef x b) )
  | OR(a, b) -> OR( (remplace_formule_coef_x coef x a), (remplace_formule_coef_x coef x b) )
  | ATOM(r) -> ATOM( replace_relation_coef_x coef x r )
  
let replace_formule_coef_x coef x f =
  let incomplet = remplace_formule_coef_x coef x f in
  AND(incomplet, ATOM(DIVIDE(coef, x)) )
  
    
(*Rends unitaire les occurences de la variable x dans la formule f*)  
let step_1 x f =
  print_string"---------------------------------------------DEBUT STEP 1 ------------------";
  print_newline();
  print_string("formule au début :"^(display_formule f));
  print_newline();
  let x_coefs = get_var_coef_from_formule x f in
  let pp = ppcm_all x_coefs in
  let mult = 
    if pp == 1 then 
      f
    else 
      mult_x_relation_in_formule pp x f in
  let replace = replace_formule_coef_x pp x mult in
  print_newline();
  print_string("formule au final :"^(display_formule replace));
  print_newline();
  print_string "------------------------------------------FIN STEP 1---------------------------------";
    
  replace, pp

(*---------------------------------------------------------Fin REPLACE--------------------------------------------------*)


(*-----------------------------------------------------------FIN Code première version--------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------*)















let neg_divide n exp =
  let rec aux k acc renvoi exp =
    match acc with
    | a when a<k -> aux k (acc+1) (OR(renvoi, ATOM(DIVIDE(acc, ADD(exp, CONST(acc)))))) exp
    | _ -> renvoi
  in
  aux n 1 NONE exp
  
let elimination_relation rela aSet bSet negation =
  match rela, negation with
  | TRUE, true -> (aSet, bSet)
  | TRUE, false -> (aSet, bSet)
  | FALSE, true -> (aSet, bSet)
  | FALSE, false -> (aSet, bSet)
  | DIVIDE(i, b), true -> (aSet, bSet)
  | DIVIDE(i, b), false -> (aSet, bSet)
  | HIGHER(a, b), true -> (aSet, b::bSet)
  | HIGHER(a, b), false -> ((ADD(b, CONST(1)))::aSet, bSet)
  | LOWER(a, b), true -> (b::aSet, bSet)
  | LOWER(a, b), false -> (aSet, (ADD(b, CONST(-1)))::bSet)
  | EQUAL(a, b), true -> ((ADD(b, CONST(1)))::aSet, (ADD(b, CONST(-1)))::bSet)
  | EQUAL(a, b), false -> (b::aSet, b::bSet)
                        
                        
let rec elimination_formule form (aSet, bSet) negation=
  match form with
  | NONE -> (aSet, bSet)
  | ATOM(a) -> elimination_relation a aSet bSet negation
  | AND(a,b) -> elimination_formule a (elimination_formule b (aSet, bSet) negation) negation 
  | OR(a,b) -> elimination_formule a (elimination_formule b (aSet, bSet) negation) negation
  | NOT (a) -> elimination_formule a (aSet, bSet) (not negation)

let rec appartient liste elem =
  match liste with
  |[] -> false
  |x::y -> if (exp_same x elem) then true else appartient y elem

         
let supprime_doublon (aSet, bSet) =
  let rec aux_liste accu liste =
    match liste with
    |[] -> accu
    |x::y -> if appartient accu x then aux_liste accu y else aux_liste (x::accu) y
  in (aux_liste [] aSet, aux_liste [] bSet)
             
let get_Set a =
  match a with
  | FREE(b) -> supprime_doublon(elimination_formule b ([],[]) true)
  | _ -> failwith" mauvais appel à getSET"


(*---------------------------------------------------- theoreme B------------------------------------------------------------------------*)

let neg_relation rela negation =
  match rela, negation with
  | TRUE, true -> TRUE
  | TRUE, false -> FALSE
  | FALSE, true -> FALSE
  | FALSE, false -> TRUE
  | DIVIDE(i, b), true -> DIVIDE(i, b)
  | DIVIDE(i, b), false -> DIVIDE(i, b)
  | HIGHER(a, b), true -> FALSE 
  | HIGHER(a, b), false -> TRUE
  | LOWER(a, b), true -> FALSE
  | LOWER(a, b), false -> TRUE
  | EQUAL(a, b), true -> FALSE
  | EQUAL(a, b), false -> TRUE
       
let rec neg_form form  negation =
  match form with
  | NONE -> NONE
  | ATOM(a) -> ATOM(neg_relation a negation)
  | AND(a,b) -> AND(neg_form a negation, neg_form b negation)
  | OR(a,b) -> OR(neg_form a negation, neg_form b negation)
  | NOT (a) -> NOT(neg_form a (not negation))
  
let neg_infinite var pp form =
  let rec aux var pp acc renvoi form =
    match acc with
    | a when a < pp -> aux var pp (acc+1)  (OR(renvoi, neg_form (replace_var_x_formule var (CONST(a)) form)  true)) form
    | _ -> renvoi
  in
  aux var pp 1 NONE form

let rec droitedroiteB var n bSet form renvoi =
  print_string "Nouveau tour dans droitedroite B ";
  print_string (string_of_int n);
  print_string ((display_formule renvoi)^"aaa");
  print_newline();

  match bSet with
  |[] -> renvoi
  |x::y -> let temp = replace_var_x_formule var (ADD(x, (CONST(n)))) form in droitedroiteB var n y form (OR(renvoi, temp))
         
let droiteB var pp form bSet =
  print_string "Nouveau tour dans DELTA B";
  print_string (display_formule form);
  print_newline();
   let rec aux var obj acc renvoi form =
    match acc with
    | a when a <= obj -> aux var obj (acc+1) (OR(renvoi,droitedroiteB var acc bSet form NONE)) form
    | _ -> renvoi
  in
  aux var pp 1 NONE form
       
let theoremeB var pp form bSet=
  print_string "theoreme B";
  print_newline();
  let gauche = neg_infinite var pp form in
  let droite = droiteB var pp form bSet in
  print_newline();
  print_string(display_formule form);
  print_newline();
  print_string (display_formule gauche);
  print_newline();
  print_string"-----------Au dessus, la formule dans theoreme a, la partie gauche, et en dessous la partie droite-------------------------";
  print_newline();    
  print_string((display_formule droite));
  OR(gauche, droite)
                 
                 
    
(*-------------------------------------------------------------------------theoreme a-------------------------------------------------------------------------*)  
 let relation rela negation =
  match rela, negation with
  | TRUE, true -> TRUE
  | TRUE, false -> FALSE
  | FALSE, true -> FALSE
  | FALSE, false -> TRUE
  | DIVIDE(i, b), true -> DIVIDE(i, b)
  | DIVIDE(i, b), false -> DIVIDE(i, b)
  | HIGHER(a, b), true -> TRUE 
  | HIGHER(a, b), false -> FALSE
  | LOWER(a, b), true -> TRUE
  | LOWER(a, b), false -> FALSE
  | EQUAL(a, b), true -> TRUE
  | EQUAL(a, b), false -> FALSE

       
let rec form1 forma  negation =
  match forma with
  | NONE -> NONE
  | ATOM(a) -> ATOM(relation a negation)
  | AND(a,b) -> AND(form1 a negation, form1 b negation)
  | OR(a,b) -> OR(form1 a negation, form1 b negation)
  | NOT (a) -> NOT(form1 a (not negation))
  
let infinite var pp form =
  let rec aux var pp acc renvoi form =
    match acc with
    | a when a < pp -> aux var pp (acc+1)  (OR(renvoi, form1 (replace_var_x_formule var (CONST(-a)) form)  true)) form
    | _ -> renvoi
  in
  aux var pp 1 NONE form

let rec droitedroiteA var n aSet form renvoi =
  match aSet with
  |[] -> renvoi
  |x::y -> droitedroiteA var n y form (OR(renvoi, replace_var_x_formule var (ADD(x, (CONST(-n)))) form))
         
let droiteA var pp form aSet =
  print_string "Nouveau tour dans DELTA";
  let rec aux var pp acc renvoi form =
    match acc with
    | a when a < pp -> aux var pp (acc+1) (OR(renvoi,droitedroiteA var acc aSet form NONE)) form
    | _ -> renvoi
  in
  aux var pp 1 NONE form
  
  
let theoremeA var pp form aSet=
  print_string "theoreme A";
  print_newline();
  let gauche = infinite var pp form in
  let droite = droiteA var pp form aSet in
  print_newline();
  print_string (display_formule form);
  print_newline();
  print_string (display_formule gauche);
  print_newline();
  print_string"-----------Au dessus, la formule dans theoreme a, la partie gauche, et en dessous la partie droite------------------------";
  print_newline();    
  print_string((display_formule droite));
  OR(gauche, droite)
  






let rec isole_eq a b var =
  match a, b with
  |_, _ when expression_contient_x var b -> isole_eq (ADD(a, MULCONST(-1,b))) (CONST(0)) var
  |VAR(var), _ -> EQUAL(a,b)
  |CONST(x), _ -> EQUAL(a,b)
  |MULCONST(x,y), _ -> isole_eq y (CONST(0)) var 
  |ADD(x,y), _ ->
    let EQUAL(k,l) = isole_eq x (CONST(0)) var in
    let EQUAL(m,n) = isole_eq y (CONST(0)) var in
    EQUAL(ADD(k,m),ADD(l,n))

let rec isole_high a b var =
  match a, b with
  |_, _ when expression_contient_x var b -> isole_high (ADD(a, MULCONST(-1,b))) (CONST(0)) var
  |VAR(var), _ -> HIGHER(a,b)
  |CONST(x), _ -> HIGHER(a,b)
  |ADD(x,y), _ ->
    let HIGHER(k,l) = isole_high x (CONST(0)) var in
    let HIGHER(m,n) = isole_high y (CONST(0)) var in
    HIGHER(ADD(k,m),ADD(l,n))
  |MULCONST(x,y), _ ->
    match x with
    | a when a==0 -> TRUE
    | a when a < 0 ->
       let EQUAL(k,l) = isole_eq y (CONST(0)) var in
       LOWER(k,l)
    | _ ->
       let EQUAL(k,l) = isole_eq y (CONST(0)) var in
       HIGHER(k,l)
       
       
let rec isole_low a b var =
  match a, b with
  |_, _ when expression_contient_x var b -> isole_low (ADD(a, MULCONST(-1,b))) (CONST(0)) var
  |VAR(var), _ -> LOWER(a,b)
  |CONST(x), _ -> LOWER(a,b)
  |ADD(x,y), _ ->
    let LOWER(k,l) = isole_low x (CONST(0)) var in
    let LOWER(m,n) = isole_low y (CONST(0)) var in
    LOWER(ADD(k,m),ADD(l,n))
  |MULCONST(x,y), _ ->
    match x with
    | a when a==0 -> TRUE
    | a when a < 0 ->
       let EQUAL(k,l) = isole_eq y (CONST(0)) var in
       HIGHER(k,l)
    | _ ->
       let EQUAL(k,l) = isole_eq y (CONST(0)) var in
       LOWER(k,l)
       
      
       
       
let isoler_relation rela var =
  match rela with
  | TRUE -> TRUE
  | FALSE -> FALSE
  | HIGHER (a, b) -> isole_high a b var
  | LOWER (a, b) -> isole_low a b var
  | EQUAL (a, b) -> isole_eq a b var
  | DIVIDE (a, b) -> DIVIDE(a,b)


let rec isoler form var=
  match form with
  | NONE -> NONE
  | ATOM(a) -> ATOM(isoler_relation a var)
  | NOT(a) -> NOT(isoler a var)
  | AND(a, b) -> AND ((isoler a var), (isoler b var))
  | OR(a, b) -> OR ((isoler a var), (isoler b var))


(*------------------------------------------------------Code A Appeler-------------------------------------------*)





let rec algo2 f =
  print_newline();
  print_newline();
  print_string((display_prenex f));
  print_newline();
  print_string"-----------------Debut de algo2 sur formule ci-dessus-----------------";
  print_newline();    
  
  match f with
  | FREE(a) -> a
  | UNIV(a, b)-> algo2( NOT(EXIST(a,(NOT(b)))) )
  | NOT (a) -> NOT(algo2 a)
  | EXIST(a, b) -> match b with
                   | FREE (x)->
                      let x2 = isoler x (VAR(a)) in
                      let replace, pp = step_1 (VAR(a)) (simplifier_formule x2) in
                      let aSet, bSet = get_Set (FREE(replace)) in
                      print_newline();
                      print_string (" list A: "^(display_list aSet));
                      print_newline();
                      print_string (" list B: "^(display_list bSet));
                      print_newline();
                      if length aSet <= length bSet
                      then theoremeA (VAR(a)) pp replace aSet
                      else theoremeB (VAR(a)) pp replace bSet
                   | _ -> algo2 (EXIST(a, (FREE(algo2 b))))


let algo f =
  let prenex = (FREE(algo2 f)) in
  print_newline();
  print_string "----------------------forme prenexe avant evaluation et simplification------------";
  print_newline();
  print_string (display_prenex prenex);
  simplifier_prenex (evaluer_prenex prenex)

    
    
    
