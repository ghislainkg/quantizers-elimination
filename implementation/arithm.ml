
let rec diviseurs_aux n i accu = 
	if i < n then
		if (n mod i) == 0 then
			diviseurs_aux n (i+1) (i::accu)
		else
			diviseurs_aux n (i+1) accu
	else 
		accu;;
let divieurs n = 
	diviseurs_aux n 1 [];;
let pgcd n m = 
	if n == 1 || m == 1 then 1
	else
		let ndivs = divieurs n in
		let mdivs = divieurs m in
		List.find
		(fun dn -> 
			(match (List.find_opt (fun dm -> (dn == dm)) mdivs) with
			| None -> false
			| Some(_) -> true)
		)
		ndivs;;

let ppcm n m =
  let pgcdnm = pgcd n m in
  (n * m) / pgcdnm;;

let rec ppcm_all_aux l accu =
  match l with
  | i::ls -> ppcm_all_aux ls (ppcm accu i)
  | [] -> accu;;

(*Determine le ppcm de tous les entiers contenu dans la liste l*)
let ppcm_all l =
  ppcm_all_aux l 1

