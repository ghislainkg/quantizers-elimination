#!/bin/bash

ocamlopt -c typage.ml
ocamlopt -c arithm.ml
ocamlopt -c debug.ml
ocamlopt -c analyseur.ml
ocamlopt -c main.ml
ocamlopt -o elimination typage.cmx arithm.cmx debug.cmx analyseur.cmx main.cmx

rm *.o
rm *.cm*
