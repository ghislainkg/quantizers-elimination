open Typage;;



let rec display_expression expr = 
	match expr with
	| CONST(i) -> string_of_int i
	| MULCONST(i, a) -> (string_of_int i)^(display_expression a)
	| VAR(a) -> a
	| ADD(a, b) -> (display_expression a)^"+"^(display_expression b);;

let display_relation rel = 
	match rel with
	| HIGHER(a, b) -> (display_expression a)^"<"^(display_expression b)
	| LOWER(a, b) -> (display_expression a)^">"^(display_expression b)
	| DIVIDE(i, b) -> (string_of_int i)^"|"^(display_expression b)
        | EQUAL (a, b) -> (display_expression a)^"="^(display_expression b)
	| TRUE -> "true"
	| FALSE -> "false";;

let rec display_formule formule = 
	let filter_none a b separateur = 
		if a == NONE && (not (b==NONE)) then
			(display_formule b)
		else if (not (a==NONE)) && b == NONE then 
			(display_formule a)
		else
			"("^(display_formule a)^separateur^(display_formule b)^")" in
	match formule with
	| NONE -> ""
	| NOT(a) -> "Not("^(display_formule a)^")"
	| AND(a, b) -> filter_none a b " and "
	| OR(a, b) -> filter_none a b " or "
	| ATOM(a) -> display_relation a;;

let rec display_prenex pre = 
	match pre with
	| FREE(f) -> display_formule f
	| UNIV(x, p) -> "V"^x^" "^display_prenex p
	| EXIST(x, p) -> "]"^x^" "^display_prenex p
        | NOT (x) -> "ø"^display_prenex x;;

let rec display_list l=
  match l with
  |[] -> "||||"
  |x::y -> "|"^(display_expression x)^ (display_list y);;

(*--------------------------------------------------------------
let rec display_expression_final expr k = 
	match expr with
	| CONST(i) -> string_of_int i
	| MULCONST(i, a) -> (string_of_int i)^(display_expression_final a)
	| VAR(a) -> a
	| ADD(a, b) -> (display_expression a)^"+"^(display_expression b);;

let display_relation_final rel k = 
	match rel with
	| HIGHER(a, b) -> (display_expression a)^"<"^(display_expression b)
	| LOWER(a, b) -> (display_expression a)^">"^(display_expression b)
	| DIVIDE(i, b) -> (string_of_int i)^"|"^(display_expression b)
        | EQUAL (a, b) -> (display_expression a)^"="^(display_expression b)
	| TRUE -> "true"
	| FALSE -> "false";;

let rec display_formule_final formule k= 
	let filter_none a b separateur = 
		if a == NONE && (not (b==NONE)) then
			(display_formule b)
		else if (not (a==NONE)) && b == NONE then 
			(display_formule a)
		else
			"("^(display_formule a)^separateur^(display_formule b)^")" in
	match formule with
	| NONE -> ""
	| NOT(a) -> "Not("^(display_formule a)^")"
	| AND(a, b) -> filter_none a b " and "
	| OR(a, b) -> filter_none a b " or "
	| ATOM(a) -> display_relation a;;

let rec display_prenex_final pre k= 
	match pre with
	| FREE(f) -> display_formule f
	| UNIV(x, p) -> "V"^x^" "^display_prenex p
	| EXIST(x, p) -> "]"^x^" "^display_prenex p
        | NOT (x) -> "ø"^display_prenex x;;
 *)
