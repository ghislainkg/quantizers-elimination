open Typage
open Analyseur;;
open Debug;;
open Arithm;;
open List;;








let exemple =
  AND(
      AND(
          ATOM(
              HIGHER(
                  MULCONST(
                      2,
                      VAR("x")
                    ),
                  CONST(5)
                )
            ),
          ATOM(
              LOWER(
                  VAR("x"),
                  CONST(20)
                )
            )
        ),
      ATOM(
          EQUAL(
              VAR("x"),
              CONST(9)
            )
        )
    )
  


let exemple1 = AND( ATOM(HIGHER(MULCONST(5, VAR("a")), VAR("b"))), ( OR( ATOM(HIGHER(VAR("b"), VAR("c"))), ATOM(DIVIDE(1, VAR("b") )) ) ) );;
let exemple2 =
	AND(
		ATOM( HIGHER( ADD(VAR("x"), CONST(5)), CONST(1) )),
		OR( 
			ATOM( HIGHER( ADD( MULCONST(13, VAR("x")), VAR("y") ), CONST(1) )),
			OR(
				ATOM(HIGHER(VAR("x"), CONST(5))),
				OR (
					ATOM(HIGHER(VAR("x"), CONST(45))),
					ATOM(HIGHER(VAR("x"), CONST(5)))
				)
			)
		)
	);;

let exemple3 =
	AND (
		ATOM( HIGHER(VAR("x"), CONST(5)) ),
		ATOM( LOWER(VAR("x"), VAR("y")) )
	)

let exemple4 =
  OR(
      AND (
	  AND (
	      ATOM(LOWER(
		       ADD(VAR("x"), MULCONST(2, VAR("y"))),
		       CONST(1)
		)),
	      ATOM(LOWER(
		       ADD(MULCONST(3, VAR("x")),MULCONST(1, VAR("y"))),
		       CONST(1)
		))
	    ),
	  ATOM(HIGHER(
                   VAR("x"),
		   CONST(2)
                 )
            )
	),
        ATOM(EQUAL(
                 VAR("z"),
                 CONST(4))
        )
    )

let exemple5 = (EXIST ("y",
                        (EXIST("x",
                               FREE(
                                   AND(
                                       ATOM(
                                           HIGHER(
                                               ADD(
                                                   VAR("x"),
                                                   MULCONST(
                                                       5,
                                                       VAR ("y")
                                                     )
                                                 ),
                                               CONST(1)
                                             )
                                         ),
                                       AND(
                                           ATOM(
                                               HIGHER(
                                                   ADD(
                                                       MULCONST(
                                                           13,
                                                           VAR("x")
                                                         ),
                                                       MULCONST(
                                                           -1,
                                                           VAR ("y")
                                                         )
                                                     ),
                                                   CONST(1)
                                                 )
                                             ),
                                           ATOM(
                                               LOWER(
                                                   ADD(
                                                       VAR("x"),
                                                       CONST(2)
                                                     ),
                                                   CONST(0)
                                                 )
                                             )
                                         )
                                     )
                                 )
                           )
                        )
                   )
            );;

print_string "-------------------------------------------------------------------NEW ALGO------------------------------------";;
print_newline();;
let exec = algo (NOT(EXIST("z",EXIST("y", EXIST("x",FREE(exemple4))))));;
 
(*let exe = algo2 exemple5;;*)
print_newline();
print_newline();
print_string("RESULT : \n");;
print_string(display_prenex exec);;
print_newline();;
































(*
let exemple_prenex = UNIV("c", UNIV("b", UNIV("a", FREE(exemple3))));;

print_string("LA FORMULE : "^( display_formule exemple3 ));;
print_newline();;

print_string("PREPARATIONS");;
print_newline();;
let replace, pp = step_1 (VAR("x")) exemple3;;
print_string(display_formule replace);;
print_newline();;

print_string("APPLIQUONS LE THEOREME");;
print_newline();;
let droit = theorem_droit pp (VAR("x")) replace;;
print_string(display_formule droit);;
print_newline();;

print_string("LA PARTIE GAUCHE DU RESULTAT EST ");;
print_newline();;
let gauche = theorem_gauche pp (VAR("x")) replace;;
print_string(display_formule gauche);;
print_newline();;

print_string("LE RESULTAT FINAL EST : ");;
print_newline();;
let result = OR (gauche, droit);;
print_string(display_formule result);;
print_newline();;


print_newline();;
print_newline();;
print_newline();;
print_newline();;
 *)
(*
print_string("formule : "^( display_formule exemple1 ));;
print_newline();;
let exe = algo (VAR("x")) exemple1;;
print_string(display_formule exe);;
print_newline();;

print_string("\n");;
print_string("formule : "^( display_formule exe ));;
print_newline();;
 *)
(*let exe = algo2 (EXIST(
                     "x",
                     EXIST(
                         "y",
                         EXIST(
                             "z",
                             AND(
                                 HIGHER(
                                     ADD(
                                         VAR("x")
                             ;;*)
(*
print_string("RESULT : \n");;
print_string(display_formule exe);;
print_newline();;
 *)



