(*Definissons les formules algebriques*)
type expression =
  | CONST of int
  | VAR of string
  | MULCONST of int * expression
  | ADD of expression * expression;;

(*Definissons les relations algebriques*)
type relation = 
  | TRUE
  | FALSE
  | HIGHER of expression * expression
  | LOWER of expression * expression
  | EQUAL of expression * expression
  | DIVIDE of int * expression ;;

(*Definissons les formules relationnelles*)
type formule = 
  | NONE
  | ATOM of relation
  | NOT of formule
  | AND of formule * formule
  | OR of formule * formule;;

type prenex = 
  | FREE of formule
  | UNIV of string * prenex
  | EXIST of string * prenex
  | NOT of prenex;;
